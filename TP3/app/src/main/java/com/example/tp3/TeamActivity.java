package com.example.tp3;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;
    private ImageView imageBadge;
    private Bitmap bitmap;
    private Team team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(Team.TAG)) {

                team = intent.getParcelableExtra(Team.TAG);
                if (team != null) {
                    updateView();
                }
            }
        }

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AsyncTaskUpdatingTeam asyncTaskTeam = new AsyncTaskUpdatingTeam();
                asyncTaskTeam.execute();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        if(team.getTeamBadge() != null) {
            AsyncTaskGettingBitmapFromUrl asyncTaskImage = new AsyncTaskGettingBitmapFromUrl();
            asyncTaskImage.execute();
        }
    }

    class AsyncTaskUpdatingTeam extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] objects) {

            URL url1;
            JSONResponseHandlerTeam JSONresp1 = new JSONResponseHandlerTeam(team);
            try {
                url1 = WebServiceUrl.buildSearchTeam(team.getName());
                JSONresp1.readJsonStream(url1.openConnection().getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            URL url2;
            JSONResponseHandlerRanking JSONresp2 = new JSONResponseHandlerRanking(team);
            try {
                url2 = WebServiceUrl.buildGetRanking(team.getIdLeague());
                JSONresp2.readJsonStream(url2.openConnection().getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            URL url3;
            JSONResponseHandlerLastEvents JSONresp3 = new JSONResponseHandlerLastEvents(team);
            try {
                url3 = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                JSONresp3.readJsonStream(url3.openConnection().getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object result){
            updateView();
            Intent intent2 = new Intent();
            intent2.putExtra(Team.TAG, team);
            setResult(RESULT_OK, intent2);
        }
    }

    class AsyncTaskGettingBitmapFromUrl extends AsyncTask {

        @Override
        protected Bitmap doInBackground(Object[] objects) {

            URL urlImageBadge;
            try {
                urlImageBadge = new URL(team.getTeamBadge());
                bitmap = BitmapFactory.decodeStream(urlImageBadge.openConnection().getInputStream());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object result){
            imageBadge.setImageBitmap(bitmap);
        }
    }
}
