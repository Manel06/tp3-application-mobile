package com.example.tp3;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    SportDbHelper dbHelper;
    /*SimpleCursorAdapter adapter;
    ListView listview;
    Cursor cursor;*/
    TeamAdapter adapter;
    RecyclerView recyclerview;
    List<Team> liste;
    SwipeRefreshLayout swipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        dbHelper = new SportDbHelper(this);

        /*SQLiteDatabase db = dbHelper.getReadableDatabase();
        dbHelper.onUpgrade(db, 1, 1);
        dbHelper.populate();*/
        if (dbHelper.fetchAllTeams().getCount() < 1) {
            dbHelper.populate();
        }

        /*adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllTeams(),
                new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.COLUMN_LEAGUE_NAME },
                new int[] {android.R.id.text1, android.R.id.text2}, 0);

        listview = (ListView) findViewById(R.id.listview);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor itemTeam = (Cursor) parent.getItemAtPosition(position);

                Intent intent = new Intent(MainActivity.this, TeamActivity.class);
                intent.putExtra(Team.TAG, dbHelper.cursorToTeam(itemTeam));
                startActivityForResult(intent,2);
            }
        });*/

        liste = dbHelper.getAllTeams();

        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        adapter = new TeamAdapter(liste);

        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerview.setAdapter(adapter);

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerview);

        final FloatingActionButton ajout = (FloatingActionButton) findViewById(R.id.fab);
        ajout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, NewTeamActivity.class);
                startActivityForResult(intent,1);
            }
        });

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipelayout);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                AsyncTaskUpdatingAllTeams asyncTaskTeams = new AsyncTaskUpdatingAllTeams();
                asyncTaskTeams.execute();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeLayout.setRefreshing(false);
                    }
                }, 10000);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1  && resultCode  == RESULT_OK) {
            if (data.hasExtra(Team.TAG)) {
                Team team = data.getParcelableExtra(Team.TAG);
                dbHelper.addTeam(team);

                /*cursor = dbHelper.fetchAllTeams();
                adapter.changeCursor(cursor);*/

                liste = dbHelper.getAllTeams();
                adapter.setFilter(liste);
            }
        }
        if (requestCode == 2  && resultCode  == RESULT_OK) {
            if (data.hasExtra(Team.TAG)) {
                Team team = data.getParcelableExtra(Team.TAG);
                dbHelper.updateTeam(team);

                /*cursor = dbHelper.fetchAllTeams();
                adapter.changeCursor(cursor);*/

                liste = dbHelper.getAllTeams();
                adapter.setFilter(liste);
            }
        }
    }

    class AsyncTaskUpdatingAllTeams extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] objects) {

            for(int i=0; i<liste.size(); i++) {

                Team team = liste.get(i);

                URL url1;
                JSONResponseHandlerTeam JSONresp1 = new JSONResponseHandlerTeam(team);
                try {
                    url1 = WebServiceUrl.buildSearchTeam(team.getName());
                    JSONresp1.readJsonStream(url1.openConnection().getInputStream());

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                URL url2;
                JSONResponseHandlerRanking JSONresp2 = new JSONResponseHandlerRanking(team);
                try {
                    url2 = WebServiceUrl.buildGetRanking(team.getIdLeague());
                    JSONresp2.readJsonStream(url2.openConnection().getInputStream());

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                URL url3;
                JSONResponseHandlerLastEvents JSONresp3 = new JSONResponseHandlerLastEvents(team);
                try {
                    url3 = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                    JSONresp3.readJsonStream(url3.openConnection().getInputStream());

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                dbHelper.updateTeam(team);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object result){
            /*cursor = dbHelper.fetchAllTeams();
            adapter.changeCursor(cursor);
            adapter.notifyDataSetChanged();*/
            adapter.setFilter(liste);
        }
    }

    class TeamAdapter extends RecyclerView.Adapter<RowHolder> {

        List<Team> teams;

        public TeamAdapter(List<Team> teams) {
            this.teams = teams;
        }

        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
            return new RowHolder(view);
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            Team team = teams.get(position);
            holder.bind(team);
        }

        public void setFilter(List<Team> teams) {
            this.teams = new ArrayList<>();
            this.teams.addAll(teams);
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return teams.size();
        }

        public boolean onItemMove(int fromPosition, int toPosition) {
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(teams, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(teams, i, i - 1);
                }
            }
            notifyItemMoved(fromPosition, toPosition);
            return true;
        }

        public void onItemRemove(int position) {

            long id = teams.get(position).getId();
            dbHelper.deleteTeam(id);

            teams.remove(position);
            notifyItemRemoved(position);
        }
    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        long id;
        TextView league;
        ImageView image;
        String url;
        TextView nameTeam;
        TextView lastEvent;

        RowHolder(View row){
            super(row);
            league = (TextView)row.findViewById(R.id.league);
            image = (ImageView)row.findViewById(R.id.icone);
            nameTeam = (TextView)row.findViewById(R.id.nameTeam);
            lastEvent = (TextView)row.findViewById(R.id.lastEvent);
            row.setOnClickListener(this);
        }

        public void bind(Team team){

            id = team.getId();
            league.setText(team.getLeague());
            nameTeam.setText(team.getName());
            lastEvent.setText(team.getLastEvent().toString());

            if(team.getTeamBadge() != null) {
                url = team.getTeamBadge();
                AsyncTaskGettingBitmapFromUrl asyncTaskImage = new AsyncTaskGettingBitmapFromUrl();
                asyncTaskImage.execute();
            }
        }

        @Override
        public void onClick(View view) {
            Team team = dbHelper.getTeam(id);
            Intent intent = new Intent(MainActivity.this, TeamActivity.class);
            intent.putExtra(Team.TAG, team);
            startActivityForResult(intent,2);
        }

        class AsyncTaskGettingBitmapFromUrl extends AsyncTask {

            @Override
            protected Bitmap doInBackground(Object[] objects) {

                Bitmap bitmap = null;
                URL urlImageBadge;
                try {
                    urlImageBadge = new URL(url);
                    bitmap = BitmapFactory.decodeStream(urlImageBadge.openConnection().getInputStream());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return bitmap;
            }

            @Override
            protected void onPostExecute(Object result){
                Bitmap bitmap = (Bitmap) result;
                image.setImageBitmap(bitmap);
            }
        }
    }

    public class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {

        TeamAdapter mAdapter;

        public SimpleItemTouchHelperCallback(TeamAdapter adapter) {
            mAdapter = adapter;
        }

        @Override
        public boolean isLongPressDragEnabled() {
            return true;
        }

        @Override
        public boolean isItemViewSwipeEnabled() {
            return true;
        }

        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
            int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
            return makeMovementFlags(dragFlags, swipeFlags);
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                              RecyclerView.ViewHolder target) {
            mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
            return true;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            mAdapter.onItemRemove(viewHolder.getAdapterPosition());
        }
    }
}
