package com.example.tp3;

import android.util.JsonReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;



public class JSONResponseHandlerRanking {

    private static final String TAG = JSONResponseHandlerTeam.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerRanking(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readRanking(reader);
        } finally {
            reader.close();
        }
    }

    public void readRanking(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table")) {
                readArrayRanking(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayRanking(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0;
        long myId = 0;
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("teamid")) {
                    myId = reader.nextLong();
                    if (myId == team.getIdTeam()) {
                        team.setRanking(nb + 1);
                    }
                } else if (name.equals("total")) {
                    if (myId == team.getIdTeam()) {
                        team.setTotalPoints(reader.nextInt());
                        break;
                    } else reader.nextInt();

                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }

}
